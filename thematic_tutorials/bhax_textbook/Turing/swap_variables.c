#include <stdio.h>
int main()
{
	int a = 42;
	int b = 100;
	puts("Before swap:");
	printf("a: %d\nb: %d\n\n",a,b);
	a = a+b;
	b = a-b;
	a = a-b;
	puts("After swap:");
	printf("a: %d\nb: %d\n\n",a,b);
	return 0;
}
