#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ()
{
    WINDOW *win;
    win = initscr();

    int x = 0;
    int y = 0;

    int x_add = 1;
    int y_add = 1;

    int x_max;
    int y_max;
    
    curs_set(0);

    for ( ;; ) {

        getmaxyx(win, y_max, x_max);
	clear();
        mvprintw (y,x,"O");

        refresh();

        x = x + x_add;
        y = y + y_add;

        if ( x >= x_max-1 || x <= 0 ) {
            x_add *= -1;
        }

        if ( y >= y_max-1 || y <= 0 ) {
            y_add *= -1;
        }

	usleep(40000);
    }

    return 0;
}
