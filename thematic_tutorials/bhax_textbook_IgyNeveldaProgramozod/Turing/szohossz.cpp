#include <iostream>
using namespace std;

int main() {
	int a = 1;
	int count = 0;
	while (a <<= 1) {
		count++;
	}
	cout << "bit counter: " << count << endl;
	cout << "I could bitshift the variable \"a\" " << count << " times before it flipped back to zero. That means it is stored on " << count+1 << " bits.\n";
	return 0;
}
