<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/ovfI1GU_yZo">https://youtu.be/ovfI1GU_yZo</link>
        </para>
        <para>
            Megoldás forrása:
            <link xlink:href="Turing/infloop_1_100w.c">
                <filename>infloop_1_100w.c</filename>
            </link>, 
            <link xlink:href="Turing/infloop_1_100f.c">
                <filename>infloop_1_100f.c</filename>
            </link>, 
            <link xlink:href="Turing/infloop_1_0.c">
                <filename>infloop_1_0.c</filename>
            </link>, 
            <link xlink:href="Turing/infloop_all_100.c">
                <filename>infloop_all_100.c</filename>
            </link>.
        </para>
        <para>
            Ahhoz, hogy egy ciklus végtelen legyen, csupán az kell, hogy a ciklusfeltétel ne teljesüljön, bármennyiszer is fut le a ciklusmagban lévő utasítássorozat. Egyszerű megoldás a ciklusfeltétel helyére egy <literal>true</literal> literált megadni, például egy <literal>while</literal> ciklusban:
        </para>
        <programlisting language="c"><![CDATA[
#include <stdbool.h>
int main()
{
    while (true);
    return 0;
}]]>
        </programlisting>
        <para>
            (írhatunk a <literal>true</literal> helyett egy egyszerű <literal>1</literal>-et is, így nincs szükségünk a stdbool header fájlra sem)
        </para>
        <para>
            Ehhez hasonló módszer a <literal>for</literal> ciklus használata a következő módon:
        </para>
        <programlisting language="c"><![CDATA[
int main()
{
    for (;;);
    return 0;
}]]>
        </programlisting>
        <para>
            Látható, hogy nincs megadva ciklusfeltétel, így a ciklusból való kilépés soha sem fog bekövetkezni. Ez a preferált módszer a fejlesztők körében. Ha ilyet látunk forráskódban, akkor azonnal tudjuk, hogy a ciklus szándékosan végtelen. Persze egy ilyen ciklusfejjel rendelkező <literal>for</literal> ciklus még nem biztos, hogy végtelen, mivel a ciklus magjában elhelyezett <literal>break;</literal> utasítás még képes véget vetni a ciklus futásának.
        </para>
        <para>
            Mindkét fenti megoldás 1 CPU magot fog 100%-ban dolgoztatni egészen addig, amíg kézzel le nem állítjuk a program futását.
        </para>
        <para>
            Ha egy magot szeretnénk 0%-ban dolgoztatni, olyan programra lesz szükség, amely bár fut, valójában nem használja a CPU-t. Erre tökéletes megoldás a <literal>sleep</literal> rendszerhívás, mely az általunk megadott ideig szünetelteti a program futását. Ennek használatához szükségünk lesz (UNIX-féle rendszer esetén) a unistd header fájl tartalmára, mely javarészt olyan függvényeket tartalmaz, melyeken keresztül rendszerhívásokat tudunk intézni a kernelhez. Íme a megoldás:
        </para>
        <programlisting language="c"><![CDATA[
#include <unistd.h>
int main()
{
    for (;;)
    {
        sleep(1);
    }
    return 0;
}]]>
        </programlisting>
        <para>
            Jelen esetben 1 másodpercig alszik a programunk, mielőtt újra arra nem kérnénk, hogy aludjon még 1 másodpercet. Aztán még egyet. Persze nem kell feltétlenül 1 másodpercenként felkelteni, altathatnánk akár tízezer másodpercig is (a határ az unsigned int maximális mérete, mely platformtól függően változhat, általában 4 294 967 295), szerencsére manuálisan is leállíthatjuk a programot alvás közben, például a már megszokott SIGINT jelzéssel (általában Ctrl+C a terminálban).
        </para>
        <para>
            Ahhoz, hogy több CPU magot is használjon egyszerre a programunk, szálakra (thread) kell bontanunk, melyek egyidejűleg fognak futni külön CPU magokon. Ehhez szükségünk lesz egy multithreading implementációra. A mi egyszerű feladatunkhoz bőven elég lesz a magas szintű OpenMP API, melynek jelen esetben elég lesz egy preprocessor direktíva segítségével megmondani, mely kódrészletet akarjuk több szálon futtatni, és ő majd létrehoz annyi szálat, ahány CPU maggal rendelkezik a számítógép, majd elindítja őket egyszerre.
        </para>
        <programlisting language="c"><![CDATA[
#include <omp.h>
int main()
{
    #pragma omp parallel
    {
        for (;;);
    }
    return 0;
}]]>
        </programlisting>
        <para>
            A gcc-vel való fordításhoz használnunk kell az <literal>-fopenmp</literal> opciót. Futás közben a <literal>top</literal> paranccsal, majd az 1-es billentyű lenyomásával megfigyelhetjük, ahogy 100%-on dolgozik az összes mag.
        </para>
    </section>        
        
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne vlgtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[Program T100
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	main(Input Q)
	{
		Lefagy(Q)
	}
}]]></programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[Program T1000
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	boolean Lefagy2(Program P)
	{
		 if(Lefagy(P))
			return true;
		 else
			for(;;); 
	}

	main(Input Q)
	{
		Lefagy2(Q)
	}

}]]></programlisting>            
        <programlisting><![CDATA[]]></programlisting>            
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?
                                
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.                
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>
    </section>        
                
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/3hZPtbDslgo">https://youtu.be/3hZPtbDslgo</link>, <link xlink:href="https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk">https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk</link>
        </para>
        <para>
            Megoldás forrása: 
            <link xlink:href="Turing/swap_variables.c">
                <filename>swap_variabless.c</filename>
            </link>, 
            <link xlink:href="Turing/swap_variables_xor.c">
                <filename>swap_variables_xor.c</filename>
            </link>.
        </para>
        <para>
            Két <literal>int</literal> típusú változó értékét nem bonyolult dolog felcserélni:
        </para>
        <programlisting language="c"><![CDATA[
#include <stdio.h>
int main()
{
        int a = 42;
        int b = 100;
        puts("Before swap:");
        printf("a: %d\nb: %d\n\n",a,b);
        int c = a;
        a = b;
        b = c;
        puts("After swap:");
        printf("a: %d\nb: %d\n\n",a,b);
        return 0;
}
]]>
        </programlisting>
        <para>
            Ez a legegyszerűbb megoldás. Látható, hogy ha <literal>a</literal> értékét felcserélem <literal>b</literal>-re, <literal>a</literal> eredeti értéke elveszett, már nem tudom odaadni <literal>b</literal>-nek. Ennek kiküszöbölésére bevezetünk egy segédváltozót (<literal>c</literal>), mely eltárolja nekünk <literal>a</literal> tartalmát.
        </para>
        <para>
            Hogyan tudnánk segédváltozó nélkül ezt megtenni?
        </para>
        <programlisting language="c"><![CDATA[
#include <stdio.h>
int main()
{
        int a = 42;
        int b = 100;
        puts("Before swap:");
        printf("a: %d\nb: %d\n\n",a,b);
        a = a + b;
        b = a - b;
        a = a - b;
        puts("After swap:");
        printf("a: %d\nb: %d\n\n",a,b);
        return 0;
}
]]>
        </programlisting>
        <para>
            Egyszerűen: az összeadás és kivonás lehetőségeit kihasználva.
        </para>
    </section>                     

    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/T2EdSwGHn3k">https://youtu.be/T2EdSwGHn3k</link>, <link xlink:href="https://bhaxor.blog.hu/2018/08/28/labdapattogas">https://bhaxor.blog.hu/2018/08/28/labdapattogas</link>
        </para>
        <para>
            Megoldás forrása:
            <link xlink:href="Turing/ball_bounce.c">
                <filename>ball_bounce.c</filename>
            </link>
        </para>
        <para>
            Ez a feladat arra az időre emlékeztet, amikor a HTML5 canvas lehetőségeivel ismerkedtem, és JavaScript kóddal rajzoltam mozgó tárgyakat a böngészőablakba. Az első ilyen projekt pontosan a pattogó labda volt. Majd lett belőle nagyon sok pattogó labda, melyeket a gravitáció, az egérrel előidézett lökések és az egymásról, valamint a falról való lepattanás irányított, gyorsulással, lassulással és felületi súrlódással valamint légellenállással. Ide egyszerű koordinátákra lesz szükségünk: a labda koordinátái, valamint a kétdimenziós tér minimum és maximum koordinátái. Ütemezve fogjuk újabb és újabb pozíciókban kirajzolni a labdánkat, mely jelen esetben egy "O" karakter, miközben figyeljük, hogy elértük-e már valamelyik falat. Ha igen, a megfelelő irányt visszafordítjuk (például ha eddig mindig eggyel növeltük az X pozíciót, most mínusz eggyel növeljük, így a síkon a másik irányba fog haladni).
        </para>
        <para>
            Lásd a <link xlink:href="Turing/ball_bounce.c">
                <filename>ball_bounce.c</filename>
            </link> forrásfájlt.
        </para>
        <para>
            Ahhoz, hogy elágazások nélkül valósítsuk meg ugyanezt, valahogyan összehasonlítás nélkül is tudnunk kell, mikor értünk falhoz. Dr. Bátfai Norbert megoldása alapján: ha megfigyeljük az x és y koordinátákat, azok egy minimum és maximum érték között mozognak a következő módon:
            min, min+1, min+2, ... , max-2, max-1, max, max-1, max-2, ... , min+2, min+1, min. Ráadásul az x és y koordináták egymástól függetlenül teszik ezt, csak más intervallumon belül. Látható, hogy ilyen számsorozatot kell generálnunk, ezek lesznek a koordináták. Ehhez használhatunk maradékos osztást, viszont arra figyelnünk kell, hogy ennek eredménye egy fűrészfogszerűen változó sorozat lesz: az értékek szépen sorban növekednek, majd hirtelen lecsökkennek a minimum értékre, és újra megfelelő ütemben növekednek. Ennek kiküszöbölésére megtehetjük azt, hogy összefésülünk két ilyen sort, így az értékek emelkedése és csökkenése is egyenletes ütemben fog zajlani.
        </para>
        <programlisting language="c"><![CDATA[
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main ()
{
    int xj = 0, xk = 0, yj = 0, yk = 0;
    int mx = 80 * 2, my = 24 * 2;

    WINDOW *ablak;
    ablak = initscr();
    noecho();
    curs_set(0);
    cbreak();
    nodelay(ablak, true);

    for (;;)
    {
        xj = (xj - 1) % mx;
        xk = (xk + 1) % mx;

        yj = (yj - 1) % my;
        yk = (yk + 1) % my;

        clear ();

        mvprintw (0, 0,
                  "--------------------------------------------------------------------------------");
        mvprintw (24, 0,
                  "--------------------------------------------------------------------------------");
        mvprintw (abs ((yj + (my - yk)) / 2),
                  abs ((xj + (mx - xk)) / 2), "X");

        refresh();
        usleep(60000);
    }
    return 0;
}
]]>
        </programlisting>
        <para>
            Fordításhoz <literal>gcc</literal> esetén szükségünk lesz az <literal>-lncurses</literal> kapcsolóra illetve a <literal>libncurses5-dev</literal> csomagra Ubuntu-alapú rendszerek esetén.
        </para>
    </section>                     

    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/I1ITRd5HTUo">https://youtu.be/I1ITRd5HTUo</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/szohossz.cpp"></link>
        </para>
        <para>
            A feladat adta magát a Linus Torvalds-féle ciklusfej miatt:
        </para>
        <programlisting language="c++"><![CDATA[
#include <iostream>
using namespace std;

int main()
{
        int a = 1;
        int count = 0;
        while (a <<= 1) {
                count++;
        }
        cout << "I could bitshift the signed integer \"a\" " << count << " times before it became negative. That means it is stored on " << count+1 << " bits.\n";
        return 0;
}
]]>
        </programlisting>
        <para>
            Fontos megemlíteni, hogy a számlálónk jelen esetben 31 biteltolást fog jelezni, mivel az első helyiértéken álló bit az előjel bit, és amikor oda kerül az 1-es, egy negatív számot kapunk, mely logikailag kiértékelve hamis, így a ciklusunk véget ér. Ezért adtam hozzá egyet a számláló értékéhez. Alternatívaként a ciklusfejben hasonlíthatnánk a változónk értékét szigorúan nullához, az viszont már nem a Linus-féle BogoMIPS ciklusfej lenne.
        </para>
    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/pagerank.c"><filename>Turing/pagerank.c</filename></link>, a második előadás <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG-BHAX/Prog1_2.pdf">55-63 fólia</link>.               
        </para>
        <para>
            A PageRank algoritmus weboldalakat rangsorol, mégpedig úgy, hogy minden weboldalhoz egy számot rendel az alapján, hogy mennyi hiperhivatkozás mutat rá, valamint azok az oldalak, amelyek hivatkoznak rá, maguk hol állnak a rangsorban. Az algoritmust Larry Page és Sergey Brin (a Google alapítói) fejlesztették ki 1998-ban a Stanford Egyetemen.
        </para>
             <programlisting language="c"><![CDATA[
#include <stdio.h>
#include <math.h>

void kiir (double tomb[], int db) { 
    for (int i = 0; i < db; ++i) 
    printf("PageRank [%d]: %lf\n", i, tomb[i]); 
}

double tavolsag (double PR[], double PRv[], int n) { 
    double osszeg = 0.0; 
    for (int i = 0; i < n; ++i) 
    osszeg += (PRv[i] - PR[i]) * (PRv[i] - PR[i]); 
    return sqrt(osszeg); 
}

int main (void) {

  double L[4][4] = { {0.0, 0.0, 1.0 / 3.0, 0.0}, 
    {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0}, 
    {0.0, 1.0 / 2.0, 0.0, 0.0}, 
    {0.0, 0.0, 1.0 / 3.0, 0.0}
    };  
  double PR[4] = { 0.0, 0.0, 0.0, 0.0 }; 
  double PRv[4] = { 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0 };

  int i, j;
  for (;;) { 
      for (i = 0; i < 4; ++i)
      { 
        PR[i] = 0.0; 
        for (j = 0; j < 4; ++j) 
          PR[i] += (L[i][j] * PRv[j]);
      } 
      if (tavolsag (PR, PRv, 4) < 0.00000001) 
          break; 
      for (i = 0; i < 4; ++i) 
      PRv[i] = PR[i]; a
  }
  kiir (PR, 4); 
  return 0; 
}]]>
            </programlisting>

    </section>
                                                                                                                                                                                                                                                                                                                                                        
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan">https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R</link>
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>
    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/xbYhp9G6VqQ">https://youtu.be/xbYhp9G6VqQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R</link>
        </para>

        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
<![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    idx = which(diff==2)
    t1primes = primes[idx]
    t2primes = primes[idx]+2
    rt1plust2 = 1/t1primes+1/t2primes
    return(sum(rt1plust2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
<![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
<![CDATA[> primes=primes(13)
> primes
[1]  2  3  5  7 11 13
]]>
        </screen>
                
        <programlisting language="R">
<![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
<![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
> diff
[1] 1 2 2 4 2
]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
<![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
<![CDATA[> idx = which(diff==2)
> idx
[1] 2 3 5
]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
<![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
<![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
<![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t1primes
[1] 0.33333333 0.20000000 0.09090909
]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t2primes
[1] 0.20000000 0.14285714 0.07692308
]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
<![CDATA[> 1/t1primes+1/t2primes
[1] 0.5333333 0.3428571 0.1678322
]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
<![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
<![CDATA[>   sum(rt1plust2)
[1] 1.044023
]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
<![CDATA[x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/VkMFrgBhN1g">https://youtu.be/VkMFrgBhN1g</link>
                    </para>    
                </listitem>                
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/aF4YK6mBwf4">https://youtu.be/aF4YK6mBwf4</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>
    <section>
        <title>Vörös Pipacs Pokol/csiga folytonos mozgási parancsokkal</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/uA6RHzXH840">https://youtu.be/uA6RHzXH840</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/RedFlowerHell">https://github.com/nbatfai/RedFlowerHell</link>               
        </para>
        <para>
            
        </para>            
    </section>        

</chapter>                
