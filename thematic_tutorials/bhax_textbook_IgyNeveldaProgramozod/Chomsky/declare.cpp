#include <stdio.h>

int* my_func() { return nullptr; };

int (* ptr_return_func(int c) ) (int,int) { ++c; return nullptr; }

int main()
{
    int a;
    
    int *int_ptr;
    
    int &r = a;
    
    int t[10];
    
    int (&arr_ref)[10] = t;
    
    int *ptr_arr[10];
    
    int* (*func_ptr)();
    func_ptr = &my_func; // & optional
    
    int (*(*fnptr)(int)) (int,int);

    return 0;
    
}
