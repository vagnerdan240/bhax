%{
#include <stdio.h>
int realnum_count = 0;
%}
DIGIT	[0-9]
%%
{DIGIT}*("."{DIGIT}+)?	{++realnum_count; 
    printf("[%s %f]", yytext, atof(yytext));}
%%
int main()
{
 yylex();
 printf("The number of real numbers is %d\n", realnum_count);
 return 0;
}
