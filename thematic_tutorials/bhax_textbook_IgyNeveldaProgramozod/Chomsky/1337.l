%{
#include <stdio.h> // printf()
#include <ctype.h> // tolower()
#include <stdlib.h> // rand()
#include <time.h> // time()
#define L337SIZE (sizeof l337c1ph / sizeof (struct cipher))

struct cipher {
    char c;
    char *leet[2];
} l337c1ph [] = {
    
  {'a', {"4", "/\\"}},  {'b', {"8", "|3"}},
  {'c', {"c", "("}},    {'d', {"d", "|]"}},
  {'e', {"3", "3"}},    {'f', {"f", "ph"}},
  {'g', {"g", "6"}},    {'h', {"h", "|-|"}},
  {'i', {"1", "|"}},    {'j', {"j", "j"}},
  {'k', {"k", "|{"}},   {'l', {"l", "|_"}},
  {'m', {"m", "|\\/|"}},{'n', {"n", "/\\/"}},
  {'o', {"0", "()"}},   {'p', {"p", "/o"}},
  {'q', {"q", "q"}},    {'r', {"r", "|2"}},
  {'s', {"5", "$"}},    {'t', {"7", "7"}},
  {'u', {"u", "(_)"}},  {'v', {"v", "\\/"}},
  {'w', {"w", "VV"}},   {'x', {"x", "x"}},
  {'y', {"y", "y"}},    {'z', {"z", "2"}},
  
  {'0', {"D", "O"}},    {'1', {"I", "I"}},
  {'2', {"Z", "Z"}},    {'3', {"E", "E"}},
  {'4', {"h", "A"}},    {'5', {"S", "S"}},
  {'6', {"G", "G"}},    {'7', {"T", "T"}},
  {'8', {"X", "X"}},    {'9', {"g", "g"}}  
};
%}
%%
.	{
	  
	  char found = 0;
	  for(int i=0; i<L337SIZE; i++)
	  {
	  
	    if(l337c1ph[i].c == tolower(*yytext))
	    {
        
          int r = rand() % 2;
          
	      printf("%s", l337c1ph[i].leet[r]);
          
	      found = 1;
	      break;
	    }
	    
	  }
	  
	  if(!found)
	     printf("%c", *yytext);	  
	  
	}
%%
int main()
{
  srand(time(NULL));
  yylex();
  return 0;
}
